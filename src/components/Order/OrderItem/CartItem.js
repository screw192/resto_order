import React from 'react';

import "./CartItem.css";

const CartItem = ({name, qty, itemSum, removeItemHandler}) => {
  return (
      <div
          className="CartItem"
          onClick={removeItemHandler}
      >
        <p className="CartItemDetails">{name} x{qty}</p>
        <p className="CartItemSum">{itemSum}</p>
      </div>
  );
};

export default CartItem;