import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import CartItem from "./OrderItem/CartItem";
import {removeFromCart, setPurchasing} from "../../store/actions/cartActions";
import Button from "../UI/Button/Button";
import "./Cart.css";

const Cart = () => {
  const dispatch = useDispatch();
  const cart = useSelector(state => state.cart.cart);
  const deliveryPrice = useSelector(state => state.cart.delivery);
  const sum = useSelector(state => state.cart.cartSummary);
  const removeItemHandler = (id, sum) => dispatch(removeFromCart(id, sum));

  let cartItems = <h5>Cart is empty...</h5>;
  let totalPriceInfo = null;

  const isPurchasable = () => {
    return Object.keys(cart).length === 0;
  }

  const continueOrder = () => dispatch(setPurchasing(true));

  if (Object.keys(cart).length > 0) {
    cartItems = Object.keys(cart).map(item => {
      const itemSum = cart[item].price * cart[item].qty
      return (
          <CartItem
              key={item}
              name={cart[item].name}
              qty={cart[item].qty}
              itemSum={itemSum}
              removeItemHandler={() => removeItemHandler(item, itemSum)}
          />
      );
    });
    totalPriceInfo = (
        <p className="CartSummary">
          Total: <b>{sum + deliveryPrice}</b>
        </p>
    );
  }

  return (
      <div className="Cart">
        <h4 className="CartTitle">Cart</h4>
        {cartItems}
        <p className="CartDeliveryPrice">
          Delivery: <b>{deliveryPrice}</b>
        </p>
        {totalPriceInfo}
        <Button
            label="Place Order"
            additionalClass="order"
            handler={continueOrder}
            disabler={isPurchasable()}
        />
      </div>
  );
};

export default Cart;