import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";

import {fetchMenu} from "../../store/actions/menuActions";
import MenuItem from "./MenuItem/MenuItem";
import {addToCart} from "../../store/actions/cartActions";
import Button from "../UI/Button/Button";
import "./Menu.css";

const Menu = () => {
  const dispatch = useDispatch();
  const menu = useSelector(state => state.menu.menu);
  const addItem = (id, name, price) => dispatch(addToCart(id, name, price));

  useEffect(() => {
    dispatch(fetchMenu());
  }, [dispatch]);

  let menuList = null;
  if (Object.keys(menu).length > 0) {
    menuList = Object.keys(menu).map(key => {
      return (
          <MenuItem
              key={key}
              name={menu[key].name}
              price={menu[key].price}
              image={menu[key].image}
          >
            <Button
                label="Add to cart"
                additionalClass="addToCart"
                handler={() => addItem(key, menu[key].name, menu[key].price)}
            />
          </MenuItem>
        );
    });
  }

  return (
      <div className="Menu">
        {menuList}
      </div>
  );
};

export default Menu;