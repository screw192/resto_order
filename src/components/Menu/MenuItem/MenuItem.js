import React from 'react';

import "./MenuItem.css";

const MenuItem = props => {
  return (
      <div className="MenuItem">
        <img src={props.image} alt={props.name} className="MenuItemImage" height="105px" width="150px"/>
        <div className="MenuItemInfo">
          <p className="MenuItemName">{props.name}</p>
          <p className="MenuItemPrice">{props.price} KGS</p>
        </div>
        <div className="menuItemAction">
          {props.children}
        </div>
      </div>
  );
};

export default MenuItem;