import React from 'react';

import Backdrop from "../Backdrop/Backdrop";
import SpinnerSVG from "./482.svg";
import "./Preloader.css";

const Preloader = () => {
  return (
      <Backdrop>
        <div className="PreloaderBack">
          <img className="PreloaderImg" src={SpinnerSVG} alt=""/>
        </div>
      </Backdrop>
  );
};

export default Preloader;