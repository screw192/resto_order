import React from 'react';

import "./Button.css";

const Button = ({label, additionalClass, handler, disabler = false}) => {
  const buttonClass = ["Button", "Button_" + additionalClass];
  return (
      <button
          type="button"
          disabled={disabler}
          className={buttonClass.join(" ")}
          onClick={handler}
      >
        {label}
      </button>
  );
};

export default Button;