import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {postOrder, setPurchasing} from "../../../store/actions/cartActions";
import Backdrop from "../Backdrop/Backdrop";
import "./Modal.css";
import Button from "../Button/Button";

const Modal = () => {
  const [deliveryInfo, setDeliveryInfo] = useState({
    name: "",
    address: "",
    phone: "",
  });
  const [validInputData, setValidInputData] = useState(false);

  let orderPostData = {
    orderItemsInfo: {},
    orderDeliveryInfo: deliveryInfo,
  };

  useEffect(() => {
    const phoneRegex = new RegExp(/^0-[0-9]{3}-[0-9]{6}$/);

    if (deliveryInfo.name.length > 0 && deliveryInfo.address.length > 0 && deliveryInfo.phone.match(phoneRegex)) {
      setValidInputData(true);
    } else {
      setValidInputData(false);
    }
  }, [deliveryInfo]);

  const dispatch = useDispatch();
  const cart = useSelector(state => state.cart.cart);

  const confirmOrder = (event, order) => dispatch(postOrder(event, order));
  const cancelOrder = () => dispatch(setPurchasing(false));

  const inputChangeHandler = e => {
    const deliveryInfoCopy = {
      ...deliveryInfo,
      [e.target.name]: e.target.value,
    };

    setDeliveryInfo(deliveryInfoCopy);
  };

  Object.keys(cart).map(id => {
    return orderPostData.orderItemsInfo[cart[id].name] = cart[id].qty;
  });

  return (
      <Backdrop>
        <div className="PurchasingModal">
          <h3 className="PurchasingModalTitle">Few more steps...</h3>
          <form className="OrderForm">
            <div className="formRow">
              <label className="OrderLabel" htmlFor="orderName">Name:</label>
              <input
                  className="OrderInputField"
                  name="name"
                  id="orderName"
                  type="text"
                  onChange={inputChangeHandler}
              />
            </div>
            <div className="formRow">
              <label className="OrderLabel" htmlFor="orderAddress">Address:</label>
              <input
                  className="OrderInputField"
                  name="address"
                  id="orderAddress"
                  type="text"
                  onChange={inputChangeHandler}
              />
            </div>
            <div className="formRow">
              <label className="OrderLabel" htmlFor="orderPhone">Phone:</label>
              <input
                  className="OrderInputField"
                  name="phone"
                  id="orderPhone"
                  type="text"
                  placeholder="0-111-222333"
                  onChange={inputChangeHandler}
              />
            </div>
            <Button
                label="Confirm Order"
                additionalClass="orderConfirm"
                handler={(e) => confirmOrder(e, orderPostData)}
                disabler={!validInputData}
            />
            <Button
                label="Change order"
                additionalClass="orderCancel"
                handler={cancelOrder}
            />
          </form>
        </div>
      </Backdrop>
  );
};

export default Modal;