import axiosOrders from "../../axios-orders";

export const FETCH_MENU_REQUEST = "FETCH_MENU_REQUEST";
export const FETCH_MENU_SUCCESS = "FETCH_MENU_SUCCESS";
export const FETCH_MENU_FAILURE = "FETCH_MENU_FAILURE";

export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = menu => ({type: FETCH_MENU_SUCCESS, menu});
export const fetchMenuFailure = error => ({type: FETCH_MENU_FAILURE, error});

export const fetchMenu = () => {
  return async dispatch => {
    try {
      dispatch(fetchMenuRequest());

      const response = await axiosOrders.get("menu.json");
      dispatch(fetchMenuSuccess(response.data));
    } catch (error) {
      dispatch(fetchMenuFailure(error));
    }
  };
};