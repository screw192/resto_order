import axiosOrders from "../../axios-orders";

export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const SET_PURCHASING = "SET_PURCHASING";

export const POST_ORDER_REQUEST = "POST_ORDER_REQUEST";
export const POST_ORDER_SUCCESS = "POST_ORDER_SUCCESS";
export const POST_ORDER_FAILURE = "POST_ORDER_FAILURE";

export const addToCart = (id, name, price) => ({type:ADD_TO_CART, id, name, price});
export const removeFromCart = (id, sum) => ({type: REMOVE_FROM_CART, id, sum});
export const setPurchasing = purchasing => ({type: SET_PURCHASING, purchasing});

export const postOrderRequest = () => ({type: POST_ORDER_REQUEST});
export const postOrderSuccess = () => ({type: POST_ORDER_SUCCESS});
export const postOrderFailure = () => ({type: POST_ORDER_FAILURE});

export const postOrder = (event, order) => {
  return async dispatch => {
    event.preventDefault();
    try {
      dispatch(postOrderRequest());

      await axiosOrders.post("orders.json", order);
      dispatch(postOrderSuccess());
    } catch (error) {
      dispatch(postOrderFailure(error));
    }
  };
};