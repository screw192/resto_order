import {
  ADD_TO_CART,
  POST_ORDER_FAILURE,
  POST_ORDER_REQUEST,
  POST_ORDER_SUCCESS,
  REMOVE_FROM_CART,
  SET_PURCHASING
} from "../actions/cartActions";

const initialState = {
  cart: {},
  cartSummary: 0,
  delivery: 120,
  purchasing: false,
  loading: false,
  error: false,
  errorMessage: "",
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      if (!Object.keys(state.cart).includes(action.id)) {
        return {
          ...state,
          cart: {
            ...state.cart,
            [action.id]: {
              name: action.name,
              price: action.price,
              qty: 1
            }
          },
          cartSummary: state.cartSummary + action.price
        };
      } else {
        return {
          ...state,
          cart: {
            ...state.cart,
            [action.id]: {
              ...state.cart[action.id],
              qty: state.cart[action.id].qty + 1
            }
          },
          cartSummary: state.cartSummary + action.price
        };
      }
    case REMOVE_FROM_CART:
      const newCartItems = {...state.cart};
      delete newCartItems[action.id];
      return {
        ...state,
        cart: {...newCartItems},
        cartSummary: state.cartSummary - action.sum
      };
    case SET_PURCHASING:
      return {...state, purchasing: action.purchasing};
    case POST_ORDER_REQUEST:
      return {...state, purchasing:false, loading: true};
    case POST_ORDER_SUCCESS:
      return {...initialState};
    case POST_ORDER_FAILURE:
      return {...state, loading: false, error: true, errorMessage: action.error.message};
    default:
      return state;
  }
};

export default cartReducer;