import {FETCH_MENU_FAILURE, FETCH_MENU_REQUEST, FETCH_MENU_SUCCESS} from "../actions/menuActions";

const initialState = {
  menu: {},
  loading: true,
  error: false,
  errorMessage: "",
};

const menuReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MENU_REQUEST:
      return {...state, loading: true};
    case FETCH_MENU_SUCCESS:
      return {...state, loading: false, menu: action.menu};
    case FETCH_MENU_FAILURE:
      return {...state, loading: false, error: true, errorMessage: action.error.message};
    default:
      return state;
  }
};

export default menuReducer;