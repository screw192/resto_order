import React from "react";
import {useSelector} from "react-redux";

import Menu from "./components/Menu/Menu";
import Cart from "./components/Order/Cart";
import Preloader from "./components/UI/Preloader/Preloader";
import Modal from "./components/UI/Modal/Modal";
import "./App.css";

const App = () => {
  const isLoadingMenu = useSelector(state => state.menu.loading);
  const isLoadingCart = useSelector(state => state.cart.loading);
  const isPurchasing = useSelector(state => state.cart.purchasing);

  let preloader;
  isLoadingMenu || isLoadingCart ? preloader = <Preloader/> : preloader = null;

  let modal;
  isPurchasing ? modal = <Modal/> : modal = null;

  return (
      <div className="App">
        {preloader}
        {modal}
        <div className="container">
          <div className="AppInner">
            <Menu/>
            <Cart/>
          </div>
        </div>
      </div>
  );
};

export default App;